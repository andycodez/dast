from .ajax_spider_server import AjaxSpiderServer
from .basic_site_server import BasicSiteServer
from .dvwa_server import DVWAServer
from .multi_page_server import MultiPageServer
from .pancakes_server import PancakesServer
from .rest_api_server import RestApiServer
from .webgoat_server import WebGoatServer

server_classes = [AjaxSpiderServer, BasicSiteServer, DVWAServer, MultiPageServer, PancakesServer, RestApiServer,
                  WebGoatServer]

__all__ = ['AjaxSpiderServer',
           'BasicSiteServer',
           'DVWAServer',
           'MultiPageServer',
           'PancakesServer',
           'RestApiServer',
           'WebGoatServer',
           'server_classes']
