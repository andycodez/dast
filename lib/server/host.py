import os
import re


class Host:
    IP_ADDRESSES = r'[0-9]+(?:\.[0-9]+){3}'
    DOCKER_ENVIRONMENT = 'DOCKER_HOST'

    def name(self) -> str:
        """
        :return: host name that is to be used when accessing this host from an engineers local machine.
        """

        if self.DOCKER_ENVIRONMENT in os.environ:
            return self._extract_ip(os.environ[self.DOCKER_ENVIRONMENT])

        return 'localhost'

    def external_name(self) -> str:
        """
        :return: host name that is to be used when accessing this host from within a Docker container.
        """

        if self.DOCKER_ENVIRONMENT in os.environ:
            return self._extract_ip(os.environ[self.DOCKER_ENVIRONMENT])

        return 'host.docker.internal'

    def _extract_ip(self, docker_host: str) -> str:
        ip_addresses = re.findall(self.IP_ADDRESSES, docker_host)

        if not ip_addresses:
            raise RuntimeError(f'Failed to determine IP address from {self.DOCKER_ENVIRONMENT} {docker_host}')

        return ip_addresses[0]
