from typing import List


class APIScanPolicy:

    @classmethod
    def exclude_rules(cls) -> List[str]:
        return [
            '40014',   # Cross Site Scripting (Persistent), see https://www.zaproxy.org/docs/alerts/40014
            '40016',   # Cross Site Scripting (Persistent) - Prime, see https://www.zaproxy.org/docs/alerts/40016
            '40017',   # Cross Site Scripting (Persistent) - Spider, see https://www.zaproxy.org/docs/alerts/40017
            '42',      # Source Code Disclosure - SVN, see https://www.zaproxy.org/docs/alerts/42
            '20012',   # Anti CSRF Tokens Scanner, see https://www.zaproxy.org/docs/alerts/20012
            '20016',   # Cross-Domain Misconfiguration, see https://www.zaproxy.org/docs/alerts/20016
            '20017',   # Source Code Disclosure - CVE-2012-1823, see https://www.zaproxy.org/docs/alerts/20017
            '20018',   # Remote Code Execution - CVE-2012-1823, see https://www.zaproxy.org/docs/alerts/20018
            '40013',   # Session Fixation, see https://www.zaproxy.org/docs/alerts/40013
            '90024',   # Generic Padding Oracle, see https://www.zaproxy.org/docs/alerts/90024
            '90025',   # Expression Language Injection, see https://www.zaproxy.org/docs/alerts/90025
            '10095',   # Backup File Disclosure, see https://www.zaproxy.org/docs/alerts/10095
            '30003',   # Integer Overflow Error, see https://www.zaproxy.org/docs/alerts/30003
            '90028',   # Insecure HTTP Method, see https://www.zaproxy.org/docs/alerts/90028
            '20014',   # HTTP Parameter Pollution scanner, see https://www.zaproxy.org/docs/alerts/20014
            '40023',   # Possible Username Enumeration, see https://www.zaproxy.org/docs/alerts/40023
            '41',      # Source Code Disclosure - Git, see https://www.zaproxy.org/docs/alerts/41
            '43',      # Source Code Disclosure - File Inclusion, see https://www.zaproxy.org/docs/alerts/43
            '10107',   # Httpoxy - Proxy Header Misuse, see https://www.zaproxy.org/docs/alerts/10107
            '40026',   # Cross Site Scripting (DOM Based), see https://www.zaproxy.org/docs/alerts/40026
            '10051',   # Relative Path Confusion, see https://www.zaproxy.org/docs/alerts/10051
            '10053',   # Apache Range Header DoS (CVE-2011-3192), see https://www.zaproxy.org/docs/alerts/10053
            '10104',   # User Agent Fuzzer, see https://www.zaproxy.org/docs/alerts/10104
            '10106',   # HTTP Only Site, see https://www.zaproxy.org/docs/alerts/10106
            '10047',   # HTTPS Content Available via HTTP, see https://www.zaproxy.org/docs/alerts/10047
            '90027',   # Cookie Slack Detector, see https://www.zaproxy.org/docs/alerts/90027
            '100001',  # Unexpected Content-Type was returned, see https://www.zaproxy.org/docs/alerts/100001
            '10109',   # Modern Web Application, see https://www.zaproxy.org/docs/alerts/10109
        ]
