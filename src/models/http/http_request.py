from src.http_headers import HttpHeaders


class HttpRequest:

    def __init__(self, method: str, url: str, headers: HttpHeaders):
        self._method = method
        self._url = url
        self._headers = headers

    @property
    def method(self):
        return self._method

    @property
    def url(self):
        return self._url

    @property
    def headers(self):
        return self._headers
