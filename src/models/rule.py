from .id import ID


class RuleID(ID):
    pass


class Rule:

    def __init__(self, rule_id: str, is_enabled: bool, name: str):
        self._rule_id = rule_id
        self._is_enabled = is_enabled
        self._name = name

    def rule_id(self) -> RuleID:
        return RuleID(self._rule_id)

    def is_enabled(self) -> bool:
        return self._is_enabled

    def name(self) -> str:
        return self._name
