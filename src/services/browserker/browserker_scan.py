import json
import logging
import os
from typing import List

from src.configuration import Configuration
from src.models import Target
from src.models.errors import BrowserkerError
from src.system import System
from src.zap_gateway.zaproxy import ZAProxy
from . import BrowserkerConfigurationFile


class BrowserkerScan:
    CONFIGURATION_FILE = '/tmp/scan.toml'
    COOKIES_FILE = '/output/cookies.json'
    DOT_FILE = '/output/report.dot'
    REPORT_FILE = '/output/findings.json'

    def __init__(self, config: Configuration, target: Target, zaproxy: ZAProxy):
        self._config = config
        self._target = target
        self._zaproxy = zaproxy

    def authenticate(self) -> None:
        self._run_browserker('auth')

    def run(self) -> List[str]:
        self._set_zap_allowed_hosts()
        self._run_browserker('run', [
            '--report', self.REPORT_FILE,
            '--dot', self.DOT_FILE,
        ])
        return self._get_browserker_urls()

    def _run_browserker(self, command: str, arguments: List[str] = []) -> None:
        logging.info('Creating Browserker configuration file from DAST settings')
        BrowserkerConfigurationFile(self._config, self.CONFIGURATION_FILE).write()

        logging.info('Starting Browserker...')
        commands = [
            '/browserker/analyzer',
            command,
            '--config', self.CONFIGURATION_FILE,
            '--reportcookiespath', self.COOKIES_FILE,
        ]

        process = System().run_piped_output(commands + arguments)

        while True:
            output = process.stdout.readline().decode('utf-8')

            if output == '' and process.poll() is not None:
                break

        exit_code = process.poll()
        logging.info(f'Browserker completed with exit code {exit_code}')

        if exit_code != 0:
            raise BrowserkerError(
                f'Failure while running Browserker {exit_code}.'
                'Exiting scan',
            )

        # set latest cookies from Browserker
        self._set_zap_cookies()

    # get the URLs scanned by Browserker
    def _get_browserker_urls(self) -> List[str]:
        with open(self.REPORT_FILE) as json_file:
            data = json.load(json_file)

        return [path['url'] for path in data['audited_urls']]

    def _set_zap_cookies(self) -> None:
        try:
            if not os.path.isfile(self.COOKIES_FILE) or os.path.getsize(self.COOKIES_FILE) == 0:
                return

            with open(self.COOKIES_FILE) as json_file:
                data = json.load(json_file)
                self._zaproxy.create_zap_httpsession(data, self._target)
        finally:
            if os.path.isfile(self.COOKIES_FILE):
                os.remove(self.COOKIES_FILE)
        return

    def _set_zap_allowed_hosts(self) -> None:
        hosts = '|'.join(self._config.browserker_allowed_hosts)
        only_proxy_hosts_regexp = rf'^(?:(?!https?:\/\/({hosts})).*).$'
        self._zaproxy.set_global_exclude_urls(only_proxy_hosts_regexp)
