from zapv2 import ZAPv2 as ZAP

from src.configuration import Configuration
from src.models import Target
from src.models.errors import TargetNotAccessibleError
from src.zap_webdriver import ZapWebdriver
from .target_probe import TargetProbe


class TargetAuthenticator:

    SUCCESS_CODES = range(200, 299)

    def __init__(self, target: Target, zap: ZAP, config: Configuration, proxy: str):
        self._config = config
        self._proxy = proxy
        self._target = target
        self._zap = zap

    def authenticate(self) -> None:
        webdriver = ZapWebdriver(self._config)

        try:
            webdriver.setup_webdriver(self._proxy)
            webdriver.login(self._zap, self._target)
        finally:
            webdriver.cleanup()

        if self._config.auth_verification_url:
            check = TargetProbe(
                self._config.auth_verification_url,
                self._config,
                proxy=self._proxy,
                follow_redirects=False,
            ).send_ping()

            if check.status_code() not in self.SUCCESS_CODES:
                raise TargetNotAccessibleError(
                    f'DAST could not reach the auth verification URL {self._config.auth_verification_url}. '
                    'Exiting scan',
                )
