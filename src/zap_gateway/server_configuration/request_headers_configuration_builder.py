from typing import Dict

from src import System
from src.configuration import Configuration


class RequestHeadersConfigurationBuilder:

    def __init__(self, config: Configuration):
        self._config = config

    def build(self) -> str:
        rules = []

        for index, (name, value) in enumerate(self._headers().items()):
            rules.extend([
                '-config', f'replacer.full_list({index}).description=header_{index}',
                '-config', f'replacer.full_list({index}).enabled=true',
                '-config', f'replacer.full_list({index}).matchtype=REQ_HEADER',
                '-config', f'replacer.full_list({index}).matchstr={name}',
                '-config', f'replacer.full_list({index}).regex=false',
                '-config', f'replacer.full_list({index}).replacement={value}',
            ])

        return rules

    def _headers(self) -> Dict[str, str]:
        user_headers = self._config.request_headers or {}
        dast_headers = {'Via': f'GitLab DAST/ZAP v{System().dast_version()}'}
        return {**user_headers, **dast_headers}
