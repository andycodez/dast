import shlex


from src.configuration import Configuration


class UserConfigurationBuilder:

    def __init__(self, config: Configuration):
        self._config = config

    def build(self) -> str:
        settings = []

        if self._config.zap_other_options:
            for option in shlex.split(self._config.zap_other_options):
                settings.append(option)

        return settings
