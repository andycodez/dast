from django.db import migrations


def create_user(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    results = User.objects.filter(username='user')

    if len(results) == 0:
        user = User.objects.create_user('user', email='user@test.gitlab.com', password='password')
        user.save()


class Migration(migrations.Migration):
    initial = True

    dependencies = [('polls', '0002_create_poll')]
    operations = [migrations.RunPython(create_user)]
