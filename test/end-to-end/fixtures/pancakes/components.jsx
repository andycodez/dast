
const pancakes = [
  {
    name: "Strawberry and Banana Pancakes",
    description: "Fluffy pancakes covered in thick chocolate, banana and strawberries.",
    href: "/pancake/1",
    imageSrc: "https://images.unsplash.com/photo-1597699401474-e8714c1b7879?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
  },
  {
    name: "Chocolate and Banana Pancakes",
    description: "Large pancakes drizzled in chocolate, banana and nuts.",
    href: "/pancake/2",
    imageSrc: "https://images.unsplash.com/photo-1597699400902-763c989cfa80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
  },
  {
    name: "Fancy Pancakes",
    description: "For the folks who care more about the Instagram photo than the pancakes",
    href: "/pancake/3",
    imageSrc: "https://images.unsplash.com/photo-1522248105696-9625ba87de6e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80"
  }
];


class Pancake extends React.Component {
    render() {
        const pancake = pancakes[this.props.match.params.id - 1];

        return (
            <div className="pancake">
                <h1>{pancake.name}</h1>

                <div className="pancake-details">
                    <img className="pancake-image" src={pancake.imageSrc} width={300} height={200} />
                    <p>{pancake.description}</p>
                </div>
            </div>
        );
    }
}


class PancakeListItem extends React.Component {
    render() {
        return (
            <div className="pancake">
                <ReactRouterDOM.Link to={this.props.href}>
                    <img className="pancake-image" src={this.props.imageSrc} width={300} height={200} />
                    <label>{this.props.name}</label>
                </ReactRouterDOM.Link>
            </div>
        );
    }
}


class Pancakes extends React.Component {
    render() {
        return (
            <div className="pancakes">
                <h1>Pancakes</h1>

                <ul className="pancake-list">
                    {pancakes.map((item, index) => <PancakeListItem key={index} href={item.href} imageSrc={item.imageSrc} name={item.name} />)}
                </ul>
            </div>
        );
    }
}


class HomePage extends React.Component {
    render() {
        return (
            <div>
                <h1>Have your Pancake and eat it too</h1>
                <div>This site is developed as a Single Page Application using React and React Router.</div>
                <div>Please use the menu to navigate the site.</div>
            </div>
        );
    }
}

class NavigationMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {hidden: true};
    }

    render() {
        return (
            <div className={"navigation-menu-container " + (this.state.hidden ? 'hide' : 'show')}>
                <ul className="navigation-menu">
                    <li className="navigation-menu-item hamburger" onClick={() => this.setState({hidden: !this.state.hidden})}>
                        <i className="fas fa-bars"></i>
                    </li>
                    <li className="navigation-menu-item">
                        <ReactRouterDOM.Link to="/">Home</ReactRouterDOM.Link>
                    </li>
                    <li className="navigation-menu-item">
                        <ReactRouterDOM.Link to="/pancakes">Pancakes</ReactRouterDOM.Link>
                    </li>
                </ul>

                <div className="page-content">{this.props.children}</div>
            </div>
        );
    }
}

class Application extends React.Component {
    render() {
        return (
            <ReactRouterDOM.BrowserRouter>
                <div className="container">
                    <NavigationMenu>
                        <ReactRouterDOM.Switch>
                            <ReactRouterDOM.Route path="/pancake/:id" component={Pancake} />
                            <ReactRouterDOM.Route path="/pancakes" component={Pancakes} />
                            <ReactRouterDOM.Route path="/" component={HomePage} />
                        </ReactRouterDOM.Switch>
                    </NavigationMenu>
                </div>
            </ReactRouterDOM.BrowserRouter>
        );
    }
}

ReactDOM.render(<Application />, document.getElementById('application'));
