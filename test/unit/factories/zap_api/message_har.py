from .message_har_entry import message_har_entry


def messages_har(entries=[message_har_entry()]):
    return message_har(entries)


def message_har(entries=[message_har_entry()]):
    content = """{
        "log": {
            "creator": {
                "name": "OWASP ZAP",
                "version": "D-2020-02-10"
            },
            "entries": [""" + ','.join(entries) + """],
            "version": "1.2"
        }
    }"""

    return content.replace('\n', '')
