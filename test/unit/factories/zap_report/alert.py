from .alert_instance import alert_instance as alert_instance_factory


def alert(instances=[alert_instance_factory()],
          otherinfo='<p>The X-XSS-Protection HTTP response header allows the web server ...</p>',
          pluginid='10016',
          cwe_id='933',
          confidence='2',
          riskcode='1',
          name='Web Browser XSS Protection Not Enabled',
          solution='<p>Ensure that the web browsers XSS filter is enabled...</p>',
          desc='<p>Web Browser XSS Protection is not enabled, or is disabled...</p>',
          reference='<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>'):
    return {
        'count': str(len(instances)),
        'riskdesc': 'Low (Medium)',
        'name': name,
        'reference': reference,
        'otherinfo': otherinfo,
        'sourceid': '3',
        'confidence': confidence,
        'alert': name,
        'instances': instances,
        'pluginid': pluginid,
        'riskcode': riskcode,
        'solution': solution,
        'cweid': cwe_id,
        'desc': desc,
    }


def alert_with_instance(instance_values={}, **kwargs):
    return alert(instances=[alert_instance_factory(**instance_values)], **kwargs)
