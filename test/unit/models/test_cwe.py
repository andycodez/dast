from unittest import TestCase

from src.models import CweID


class TestCweID(TestCase):

    def test_valid_returns_true_if_id_is_non_zero(self):
        cwe_id = CweID('1')

        self.assertTrue(cwe_id.valid())

    def test_valid_returns_false_if_id_is_zero(self):
        cwe_id = CweID('0')

        self.assertFalse(cwe_id.valid())

    def test_cast_to_int_throws_accurate_error(self):
        cwe_id = CweID('0')

        with self.assertRaises(ValueError) as error:
            int(cwe_id)

        self.assertEqual(str(error.exception), 'ID must be a non-zero integer')
