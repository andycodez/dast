import unittest

from src.http_header import HttpHeader
from src.http_headers import HttpHeaders


class TestHttpHeaders(unittest.TestCase):

    def test_returns_headers_as_list_of_dictionary(self):
        http_headers = HttpHeaders([HttpHeader('User-Agent', 'python-requests/2.20.1')])
        dicts = http_headers.to_list_of_dicts()

        self.assertEqual(len(dicts), 1)
        self.assertEqual(dicts[0], {'name': 'User-Agent', 'value': 'python-requests/2.20.1'})

    def test_returns_no_dicts_when_no_headers(self):
        self.assertEqual(len(HttpHeaders([]).to_list_of_dicts()), 0)

    def test_should_sort_dicts_by_name(self):
        http_headers = HttpHeaders([HttpHeader('User-Agent', 'python-requests/2.20.1'),
                                    HttpHeader('Accept', '*/*')])
        dicts = http_headers.to_list_of_dicts()

        self.assertEqual(len(dicts), 2)
        self.assertEqual(dicts[0]['name'], 'Accept')
        self.assertEqual(dicts[1]['name'], 'User-Agent')

    def test_should_mask_dict_header_values_when_requested(self):
        http_headers = HttpHeaders([HttpHeader('Authorization', 'Bearer 1234567')])
        dicts = http_headers.mask(['Authorization']).to_list_of_dicts()

        self.assertEqual(len(dicts), 1)
        self.assertEqual(dicts[0], {'name': 'Authorization', 'value': HttpHeader.MASK})

    def test_should_mask_dict_header_values_independent_of_header_case(self):
        http_headers = HttpHeaders([HttpHeader('Authorization', 'Bearer 1234567')])
        dicts = http_headers.mask(['AUTHORIZATION']).to_list_of_dicts()

        self.assertEqual(len(dicts), 1)
        self.assertEqual(dicts[0]['value'], HttpHeader.MASK)

    def test_should_not_mask_dict_header_values_when_not_requested(self):
        http_headers = HttpHeaders([HttpHeader('Authorization', 'Bearer 1234567')])
        dicts = http_headers.mask(['User-Agent']).to_list_of_dicts()

        self.assertEqual(len(dicts), 1)
        self.assertEqual(dicts[0], {'name': 'Authorization', 'value': 'Bearer 1234567'})
